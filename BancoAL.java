import java.util.ArrayList;

public class BancoAL {

private ArrayList<Conta> banco;

    public void BancoAl() {
        banco = new ArrayList<Conta>();
    }

    public void cadastrar(Conta conta) {
        banco.add(conta);
    }

    private Conta procurar(String numero) {
        for(Conta conta : banco) {
            if (conta.getNumero().equals(numero)) {
                return conta;
            }
        }
        return null;
    }

    public void creditar(String numero, double valor) {
        Conta conta = procurar(numero);
        if (conta != null) {
            conta.creditar(valor);
        }
        else {
            System.out.println("Conta n existe");
        }
    }

    public void debitar(String numero, double valor) {
        Conta conta = procurar(numero);
        if (conta != null) {
            conta.debitar(valor);
        }
        else {
            System.out.println("Conta n existe");
        }
    }

    public double saldo(String numero) {
        Conta conta = procurar(numero);
        return conta.getSaldo();
    }

    public void transferir(String origem, String destino, double valor) {
        Conta contaOrigem = procurar(origem);
        Conta contaDestino = procurar(destino);
        if ( !(contaOrigem == null || contaDestino == null) ) {
        contaOrigem.debitar(valor);
        contaDestino.creditar(valor);
        }
    }
}