class Main {
    public static void main(String[] args) {
        Banco banco = new Banco();
        ContaEspecial conta = new ContaEspecial("123.321-E");
        banco.cadastrar(conta);
        conta.creditar(200);
        conta.renderBonus();
        System.out.println(conta.getSaldo());
    }
}