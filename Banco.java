public class Banco {

  private Conta[] contas;
  private int indice = 0;

  public Banco() {
    contas = new Conta[100];
  } 

  public void cadastrar(Conta conta) {
    contas[indice] = conta;
    indice++;
  }

  private Conta procurar(String numero) {
    int i = 0;
    
    while (i < indice) {
      if ( contas[i].getNumero().equals(numero) )
        return contas[i];
      i++;
    }
    
    return null;
  }

  public void creditar(String numero, double valor) {
    Conta conta = procurar(numero);
    if (conta != null) {
      conta.creditar(valor);
    }
    else {
      System.out.println("Conta n existe");
    }
  }

  public void debitar(String numero, double valor) {
    Conta conta = procurar(numero);
    if (conta != null) {
      conta.debitar(valor);
    }
    else {
      System.out.println("Conta n existe");
    }
  }

  public double saldo(String numero) {
    Conta conta = procurar(numero);
    return conta.getSaldo();
  }

  public void transferir(String origem, String destino, double valor) {
    Conta contaOrigem = procurar(origem);
    Conta contaDestino = procurar(destino);
    if ( !(contaOrigem == null || contaDestino == null) ) {
      contaOrigem.debitar(valor);
      contaDestino.creditar(valor);
    }
  }

  

}